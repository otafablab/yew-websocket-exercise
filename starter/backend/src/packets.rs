use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Debug, strum_macros::Display)]
#[serde(tag = "type", content = "data")]
pub enum PacketType {
    Message(String),
    Error(u64, String),
}

impl PacketType {
    pub fn to_json(&self) -> String {
        serde_json::to_string(self).unwrap()
    }
}
