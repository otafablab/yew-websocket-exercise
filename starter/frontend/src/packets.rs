use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Debug)]
#[serde(tag = "type", content = "data")]
pub enum PacketType {
    Message(String),
    Error(u64, String),
}

impl PacketType {
    pub fn to_json(&self) -> String {
        serde_json::to_string(self).unwrap()
    }
}
